<?php

namespace yaowei;

class CF7_DateTimePicker
{

    private $type, $input_name;

    function __construct($type, $name)
    {
        $this->input_name = $name;
        $this->type = in_array($type, array('date', 'time', 'datetime')) ? $type . 'picker' : 'datepicker';
    }

    public function generate_code()
    {
        $selector = "$('input[name=\"{$this->input_name}\"]')";

        $out = "{$selector}.flatpickr({ enableTime: true, minDate: \"today\" })";

        return $out;
    }

}
