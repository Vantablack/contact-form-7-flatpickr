<?php

namespace yaowei;

class ContactForm7Datepicker_Admin
{

    function __construct()
    {
        add_action('admin_enqueue_scripts', array($this, 'enqueue_assets'));
//		add_filter('wpcf7_editor_panels', array($this, 'add_panel'));
//      add_action('admin_footer', array($this, 'theme_js'));
//		add_action('wp_ajax_cf7dp_save_settings', array($this, 'ajax_save_settings'));
    }

    function enqueue_assets()
    {
        if (is_admin() && !self::is_wpcf7_page())
            return;

        ContactForm7Flatpickr::enqueue_js();
        ContactForm7Flatpickr::enqueue_css();
    }

    private static function is_wpcf7_page()
    {
        global $current_screen, $pagenow;

        if (is_object($current_screen) && strpos($current_screen->id, 'page_wpcf7'))
            return true;

        return false;
    }
}

new ContactForm7Datepicker_Admin;
