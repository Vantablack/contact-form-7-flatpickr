# Contact Form 7 Flatpickr

This plugin is a heavily modified version of [Contact Form 7 Datepicker](https://wordpress.org/plugins/contact-form-7-datepicker/).

This plugin uses [flatpickr](https://github.com/flatpickr/flatpickr) instead of jQuery UI's datepicker.

There are no generators available for this plugin so here's a basic usage

`[datetime* date-time-input placeholder "Click to open date/time picker"]`