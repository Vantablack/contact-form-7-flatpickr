<?php

namespace yaowei;

/**
 * Plugin Name: Contact Form 7 Flatpickr
 * Plugin URI:
 * Description: Easily add a date field using flatpickr to your CF7 forms. This plugin depends on Contact Form 7.
 * Author: Yaowei
 * Version: 0.0.1
 * Author URI:
 */

/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

class ContactForm7Flatpickr
{

    const FLATPICKR_VERSION = '4.4.6';

    function __construct()
    {
        add_action('plugins_loaded', array($this, 'load_modules'), 50);

        add_action('wpcf7_enqueue_scripts', array(__CLASS__, 'enqueue_js'));
        add_action('wpcf7_enqueue_styles', array(__CLASS__, 'enqueue_css'));

        register_activation_hook(__FILE__, array($this, 'activate'));

        if (is_admin()) {
            require_once dirname(__FILE__) . '/admin.php';
        }
    }

    function load_modules()
    {
        require_once dirname(__FILE__) . '/datetimepicker.php';
        require_once dirname(__FILE__) . '/modules/datetime.php';
    }

    function activate()
    {
        // Do nothing
    }

    public static function enqueue_js()
    {
        wp_enqueue_script(
            'flatpickr-js',
            plugins_url('js/flatpickr.js', __FILE__),
            array(),
            self::FLATPICKR_VERSION,
            true
        );
    }

    public static function enqueue_css()
    {
        wp_enqueue_style(
            'flatpickr-css',
            plugins_url('js/flatpickr.min.css', __FILE__),
            '',
            self::FLATPICKR_VERSION,
            'all'
        );
    }
}

new ContactForm7Flatpickr;
